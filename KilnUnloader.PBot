//init
const PBotUtils = Java.type('haven.purus.pbot.PBotUtils');
const PBotAPI = Java.type('haven.purus.pbot.PBotAPI');
const PBotGobAPI = Java.type('haven.purus.pbot.PBotGobAPI');
const PBotWindowAPI = Java.type('haven.purus.pbot.PBotWindowAPI');
const PBotCharacterAPI = Java.type('haven.purus.pbot.PBotCharacterAPI');

const SBUBBLEZ = 20;
const WINDOWNAME = "Kiln Unloader";
const LS_COAL = ["Coal"];
const LS_STOCKPILE = "Stockpile";
const LS_COLLECT = "Collect coal";
const TICK = 200;
var stopmsg = [];

//UI
speakP("Tarkiln unloader 0.1 HOWTO:\n   1. Prepare empty coal piles beside kilns\n   2.  Click [Start] and select work zone with kilns and piles");
const window = PBotUtils.PBotWindow(WINDOWNAME, 32, 120, ScriptID);
const f1Btn = window.addButton("f1F", "Start", 110, 5, 2);
const lbl1 = window.addLabel("", 30, 10);
lbl1.hide();
const f1F = () => {
	f1Btn.hide();
	lbl1.show();
	stopmsg = [];
	try {
		if ( main() ) 
			speakP("Done");
	} catch (e) {
		speakW("ERROR: " + e);
		window.closeWindow();
	}
	f1Btn.show();
	lbl1.hide();
}

function main() {
	lbl1.setText("Mark work zone");
	var aKilns = [];
	var aPiles = [];
	PBotUtils.selectArea();
	let lAllGobs = PBotUtils.gobsInArea(PBotUtils.getSelectedAreaA(), PBotUtils.getSelectedAreaB());
	for (let g of lAllGobs) {
		if (isPile(g))
			aPiles.push(g);
		if (isKiln(g))
			aKilns.push(g);
	}
	
	if (aPiles.length == 0)
		err("No piles");
	if (aKilns.length == 0)
		err("No kilns");
	if (irq()) return false;

	let pile = aPiles.pop(); 
	let kiln = aKilns.pop();
	
	lbl1.setText("Close to stop");
	do {
		while( isKiln(kiln) ) {
			loadFrom(kiln);

			while( !isPile(pile) ) {
				if (aPiles.length == 0) {
					err("Out of piles");
					break;
				}
				else
					pile = aPiles.pop();
			}				
			if (irq()) return false;

			if ( !unloadTo(pile) )
				err("Unload error");
			if (irq()) return false;

		}
		kiln = aKilns.pop();
	} while (kiln);

	if (irq()) return false;
	return true;
}
//HELPERS
function unloadItems(items) {
	let invItems = null;
	try {
		invItems = PBotUtils.playerInventory().getInventoryItemsByNames(items);
		for (let i of invItems)
			i.transferItem();
	} catch(e) {
		err("unloadItems() error: " + e);
		return false;
	}
	return true;
}
function unloadTo(a) {
	speakP("Stocking coal");
	a.pfClick(1, 4);
	if (PBotUtils.getItemAtHand() != null) {
		a.itemClick(0);
		waitForHandEmpty(true, 2 * 1000);
	}
	a.doClick(3, 0);
	let wPile = waitWindow(LS_STOCKPILE, 2 * 1000);
	if (wPile == null) {
		err("No stockpile window");
		return false;
	}
	return ( unloadItems(LS_COAL) );
}
function loadFrom(a) {
	speakP("Grabbing coal");
	a.pfClick(3, 0);
	if (!pickPetal(LS_COLLECT))
		err("loadFrom() flowermenu error");
	speakP("Coallecting started...");
	waitForIdle();
	speakP("Coallecting done");
}
function waitWindow(s, t) {
	let r = null;
	let c = 0;
	while ((r == null) && (c < t)) {
		r = PBotWindowAPI.getWindow(s);
		PBotUtils.sleep(TICK);
		c += TICK;
	}
	return r;
}
function getSDT(g) {
	const hcResDrawable = Java.type("haven.ResDrawable");
	let r = null;
	try {
		r = g.getattr(hcResDrawable).sdt.peekrbuf(0);
	} catch (e) {
		PBotUtils.sysMsg("getSDT(" + g + ") error: " + e);
	}
	return r;
}
function isKiln(a) {
	let res = false;
	try {
		if (a == null)
			return false;
		if (a.getResname() != "gfx/terobjs/tarkiln")
			return false;
		res = ( 
			(getSDT(a.gob) == 10) //no tar, coal
			||
			(getSDT(a.gob) == 42) //tar and coal
		);
	} catch(e) {speakW("isKiln: " + e)};
	return res;
}
function isPile(a) {
	let res = false;
	try {
		if (a == null)
			return false;
		if (a.getResname() != "gfx/terobjs/stockpile-coal")
			return false;
		res = (!a.stockpileIsFull());
	} catch(e) {speakW("isPile: " + e)};
	return res;
}
function pickPetal(s) {
	let r = false;
	PBotUtils.waitForFlowerMenu();
	r = PBotUtils.choosePetal(s);
	PBotUtils.closeFlowermenu();
	return r;
}
function waitForIdle() {
	const maxWaitTicks = 4;
	let idleCounter = maxWaitTicks;
	while (idleCounter >= 0 && !irq()) {
		if (PBotUtils.getHourglass() == -1)
			idleCounter--;
		else {
			idleCounter = maxWaitTicks;
		}
		PBotUtils.sleep(TICK);
		if (PBotCharacterAPI.getStamina() < 50)
			PBotUtils.drink(true);
	}
}
function waitForHandEmpty(b, t){
	let counter = 0;
	while ((PBotUtils.getItemAtHand() == null) != b) {
		if (counter >= t) {
			return false;
		}
		PBotUtils.sleep(TICK);
		counter += TICK;
	}
	return true;
}
function err(s) {
	stopmsg.push(s);
}
function speak(g, h, s) {
	PBotAPI.gui.map.glob.oc.speak(g, h, s);
	PBotUtils.sleep(TICK);
	PBotAPI.gui.map.glob.oc.speak(g, h, "");
}
function speakW(s) {
	// speak(PBotGobAPI.player().gob, SBUBBLEZ, s);
	PBotUtils.sysMsg(s, 255, 192, 0);
}
function speakP(s) {
	// speak(PBotGobAPI.player().gob, SBUBBLEZ, s);
	PBotUtils.sysMsg(s, 192, 255, 192);
}
function irq() {
	if (PBotCharacterAPI.getStamina() < 35) {
		err("Out of stamina");
	}
	if (PBotCharacterAPI.getEnergy() < 35) {
		err("Out of energy");
	}
	if (PBotWindowAPI.getWindow(WINDOWNAME) == null) {
		err("Stopped by user");
	}
	if (stopmsg.length == 0) {
		return false;
	} else {
		let errstr = "⚠ ";
		for (let r of stopmsg) {
			errstr += r + " ⚠ ";
		}
		PBotUtils.sysMsg(errstr, 255, 128, 0);
		return true;
	}
}


/*tarkiln sdt

	2 = empty
	10 = coal, no tar
	34 = tar, no coal
	42 = tar and coal

*/