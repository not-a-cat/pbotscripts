const VER = "0.1a";
const HOWTO = `
	A: MARKING TILES WITH CAVEDUST
	 A0. Grab cavedust from ground
	 A1. Stand on the tile where cavedust has dropped
	 A2. Run script to mark this tile with amount of last dust in inventory
	 A3. Run script again to remove marking you are standing on
	B: MARKING DANGEROUS TILES
	 B0. Run script with no [0.01-0.08] units of cavedust per item inside inventory
	 B1. Click/hold-and-drag tiles to mark
	 B2. Click/hold-and-drag again to remove marks
`;

const PBotAPI = Java.type('haven.purus.pbot.PBotAPI');
const PBotGobAPI = Java.type('haven.purus.pbot.PBotGobAPI');
const PBotUtils = Java.type('haven.purus.pbot.PBotUtils');
const hcCoord2d = Java.type('haven.Coord2d');
const hcCoord = Java.type('haven.Coord');
const hcResource = Java.type('haven.Resource');
const hcMessage = Java.type('haven.Message');
const hcResDrawable = Java.type('haven.ResDrawable');

const GOBIDSTART = -900000;
const TILESZ = 11.0;
const RESNAME = "gfx/terobjs/items/cavedust";
const MARKERITEM = "gfx/invobjs/cavedust";

// const d = (i) => PBotUtils.sysMsg(`${i}`);
const d = (i) => {};

main();

function main() {
	let markNumber = getMarker(MARKERITEM);
	if ( !markNumber || markNumber > 8 )
		markArea();
	else
		toggleMark(tileCenter(), `${markNumber}`);
}
function tileCenter() {
	let playerPostition = PBotGobAPI.player().gob.rc;
	let mmCenterOffset = playerPostition.mod(TILESZ, TILESZ).sub(TILESZ / 2, TILESZ / 2);
	let res = playerPostition.sub(mmCenterOffset);
	d(`Tile offset: ${mmCenterOffset}`);
	return res;
}
function markArea() {
	PBotUtils.selectArea();
	let s = {
		"a" : PBotUtils.getSelectedAreaA(),
		"b" : PBotUtils.getSelectedAreaB()
	};
	let l = {
		"min" : {
			"x" : Math.min(s.a.x, s.b.x),
			"y" : Math.min(s.a.y, s.b.y),
		},
		"max" : {
			"x" : Math.max(s.a.x, s.b.x),
			"y" : Math.max(s.a.y, s.b.y),
		},
	}
	
	for (let i = l.min.x; i < l.max.x; i += 11) {
		for (let j = l.min.y; j < l.max.y; j += 11) {
			let tile = new hcCoord2d(i, j);
			toggleMark(tile.add(TILESZ/2, TILESZ/2), null);
		}
	}
}
function toggleMark(c, l) {
	let existing = PBotGobAPI.getGobWithCoords(c);
	if ((existing) && (existing.getResname() == RESNAME))
		removeMark(existing);
	else
		setMark(c, l);
}
function removeMark(g) {
	d(`${g.getGobId()} removed`);
	PBotAPI.gui.ui.sess.glob.oc.remove(g.getGobId());
}
function setMark(c, l) {
	const jColor = Java.type('java.awt.Color');
	const hcGob = Java.type('haven.Gob');
	const hcOverlay = hcGob.Overlay;
	// const hcBPRadSprite = Java.type('haven.resutil.BPRadSprite');
	const hcFightCurrentOpp = Java.type('haven.FightCurrentOpp');
	const hcGobHitbox = Java.type('haven.GobHitbox');
	let gobId = GOBIDSTART;
	while (PBotGobAPI.findGobById(gobId))
		gobId--;

	const loadedRes = hcResource.remote().load(RESNAME);
	let ui = PBotAPI.gui.ui;
	let g = ui.sess.glob.oc.getgob(gobId, 0);
	ui.sess.glob.oc.move(g, c, Math.PI/2);
	let newattr = new hcResDrawable(g, loadedRes, hcMessage.nil);
	g.setattr(newattr);
	d(`${gobId} added`);
	if (!l) {	
		let aOL = [
			// new hcOverlay(new hcBPRadSprite(0.5, 25.0, hcBPRadSprite.smatDanger)),
			new hcOverlay(new hcFightCurrentOpp()),
			new hcOverlay(new hcGobHitbox(g, new hcCoord(-7, -7), new hcCoord(7, 7), true)),
			// new hcOverlay(new hcGobHitbox(g, new hcCoord(-4, -4), new hcCoord(4, 4), false)),
		];
		for (let ol of aOL)
			g.ols.add(ol);
	} else {
		multilineTextOL(PBotGobAPI.findGobById(gobId), l);
	}
}
function getMarker(itemname) {
	let markers = getItemsByResname(itemname);
	let lastMarker = markers[markers.length - 1];
	if (!lastMarker)
		return null;
	let weight = lastMarker.getName().substr(0, 4);
	let label = Math.round(parseFloat(weight) * 100);
	return label;
}
function getItemsByResname(s) {
	let res = [];
	let done = false;
	let TO = 10;
	do {
		try {
				let items = PBotUtils.playerInventory().getInventoryContents();
				for (let i of items) {
					if ( !i.getResname() )
						continue;
					if (i.getResname() ==s)
						res.push(i);
				}
				done = true;
		} catch (e) {
			res = [];
			done = false;
			d(`getItemsByResname: ${e}`);
			TO--;
			PBotUtils.sleep(TICK);
		}
	} while ((!done) & (TO >= 0))
	return res;
}
function multilineTextOL(gob, text) {
	const INIT = 27;
	const LSPC = 11;
	const FANCYNUMS = {
		'1': `
		█
		█
		▀`,
		'2': `
		▀▀█
		█▀▀
		▀▀▀`,
		'3': `
		▀▀█
		▀▀█
		▀▀▀`,
		'4': `
		█ █
		▀▀█
		  ▀`,
		'5': `
		█▀▀
		▀▀█
		▀▀▀`,
		'6': `
		█▀▀
		█▀█
		▀▀▀`,
		'7': `
		█▀█
		  █
		  ▀`,
		'8': `
		█▀█
		█▀█
		▀▀▀`
	};

	if ( (!FANCYNUMS[text]) ){
		gob.addGobText(text, 0);
		return false;
	}

	let lines = FANCYNUMS[text].split("\n");
	let height = INIT;
	for (let l of lines) {
		d(l);
		gob.addGobText(l, height)
		height -= LSPC;
	}
	return true;
}