//meta
const WINDOWNAME = "Tin Woodman";
const VER = "0.1a";
const ITEMSHOVEL = "Metal Shovel";
const ITEMAXE = "Woodsman's Axe";
const ACTIONCHOP = "Chop";
const HOWTO = `
Chops down trees and bushes in area, destroys stumps
  1. Put both ${ITEMAXE} and ${ITEMSHOVEL} into inventory
  2. Put some water on your toolbelt
  3. Press [${ACTIONCHOP}] to start
`;
//init
const PBotUtils = Java.type('haven.purus.pbot.PBotUtils');
const PBotAPI = Java.type('haven.purus.pbot.PBotAPI');
const PBotGobAPI = Java.type('haven.purus.pbot.PBotGobAPI');
const PBotWindowAPI = Java.type('haven.purus.pbot.PBotWindowAPI');
const PBotCharacterAPI = Java.type('haven.purus.pbot.PBotCharacterAPI');
const hcCoord = Java.type('haven.Coord');
const defCoord = new hcCoord(16, 16);
var stopmsg = [];
//cfg
const DEBUGLVL = 4;
const TICK = 120;
//ui
const mWindow = PBotUtils.PBotWindow(WINDOWNAME, 120, 110, ScriptID);
const f1b = mWindow.addButton("f1", ACTIONCHOP, 64, 5, 5);
const f2b = mWindow.addButton("f2", "?", 32, 5 + 64, 5);
const infoLabel = mWindow.addLabel("", 45, 100);
const f2 = () => {  
  PBotUtils.sysMsg(`${WINDOWNAME} ${VER} How-To: ${HOWTO}`);
}
const f1 = () => {
  stopmsg = [];
  infoLabel.setText("");
  f1b.hide();
  f2b.hide();
  let ADRINK = Java.type('haven.Config').autodrink;
  autodrinkSwitch(false);
  try {
    main();
  } catch (e) {
    dpo(`ERROR: ${e}`, 0);
    mWindow.closeWindow();
  } finally {
    autodrinkSwitch(ADRINK);
    f1b.show();
    f2b.show();
  }
  infoLabel.setText("Done");
}
function main() {
  var aTrees = [];
  var aBushes = [];
  var aStumps = [];
  PBotUtils.selectArea();
  let lAllGobs = PBotUtils.gobsInArea(PBotUtils.getSelectedAreaA(), PBotUtils.getSelectedAreaB());
  for (let g of lAllGobs) {
    if (isStump(g))
      aStumps.push(g);
    if (isBush(g))
      aBushes.push(g);
    if (isTree(g))
      aTrees.push(g);
  }
  dpo(`${aTrees.length} trees, ${aStumps.length} stumps and ${aBushes.length} bushes total`, 1);
  for (let s of aStumps) {
    if (irq()) return;
    infoLabel.setText(`S: ${aStumps.indexOf(s)} / ${aStumps.length}`);
    removeStump( s.getRcCoords() );
  }
  for (let b of aBushes) {
    if (irq()) return;
    infoLabel.setText(`B: ${aBushes.indexOf(b)} / ${aBushes.length}`);
    dpo(chopGob(b), 1);
  }
  for (let t of aTrees) {
    if (irq()) return;
    infoLabel.setText(`T: ${aTrees.indexOf(t)} / ${aTrees.length}`);
    dpo(chopGob(t), 1);
    removeStump( t.getRcCoords() );
  }
}
//helpers
function isTree(a) {
  if (!a) return false;
  let res = false;
  try {
    let gobName = a.getResname();
    if (gobName.endsWith("stump") || gobName.endsWith("log"))
      return false;
    res = gobName.startsWith("gfx/terobjs/trees/");
  } catch(e) {}
  return res;
}
function isBush(a) {
  if (!a) return false;
  let res = false;
  try {
    let gobName = a.getResname();
    res = gobName.startsWith("gfx/terobjs/bushes/");
  } catch(e) {}
  return res;
}
function isStump(a) {
  if (!a) return false;
  let res = false;
  try {
    let gobName = a.getResname();
    res = (gobName.startsWith("gfx/terobjs/trees/") && gobName.endsWith("stump"));
  } catch(e) {}
  return res;
}
function chopGob(a) {
  try {
    if ( !isEquiped(ITEMAXE) ) 
      equipTool(ITEMAXE, 6);
    let mark = a.addGobText('►  ▪  ◄', 8);
    dpo(`Reaching ${a}`, 1);
    if ( !a.pfClick(1, 0) )
      return `Unable to reach ${a}`;
    dpo(`Reached ${a} OK`, 2);
    a.removeGobText(mark);

    dpo(`Chopping ${a}`, 1);
    a.doClick(3, 0);
    if ( !pickPetal(ACTIONCHOP) )
      return `Unable to ${ACTIONCHOP}`;
    waitForIdle();
  } catch (e) {return `chopGob() e: ${e}`;}
  return null;
}
function pickPetal(s) {
  let r = false;
  PBotUtils.waitForFlowerMenu();
  r = PBotUtils.choosePetal(s);
  PBotUtils.closeFlowermenu();
  return r;
}
function waitForIdle() {
  dpo("Waiting for idle", 2);
  const maxWaitTICKs = 4;
  let idleCounter = maxWaitTICKs;
  while (idleCounter >= 0 && !irq()) {
    if (PBotUtils.getHourglass() == -1)
      idleCounter--;
    else {
      idleCounter = maxWaitTICKs;
    }
    PBotUtils.sleep(TICK);
    if ( !drink(4) )
      err(`Unable to drink`);
  }
  dpo("Idle", 2);
}
function drink(sips) {
  const SIPDELAY = 900;
  if (PBotCharacterAPI.getStamina() > (100 - sips * 10) )
    return true;
  dpo("Drinkwait ON", 2);
  if ( !PBotUtils.drink(false) )
    return false;
  PBotUtils.sleep(SIPDELAY * sips);
  dpo("Drinkwait OFF", 2);
  return true;
}
function removeStump(c) {
  let stump = null;
  stump = PBotGobAPI.getGobWithCoords(c);
  if ( !isStump(stump) )
    return `No stump at ${c}`;
  let mark = stump.addGobText('►  ▪  ◄', 8);
  stump.pfClick(1, 0);
  stump.removeGobText(mark);
  try {
    if ( !isEquiped(ITEMSHOVEL) ) 
      equipTool(ITEMSHOVEL, 6);

    dpo(`Destroying ${stump}`, 1);
    PBotCharacterAPI.doAct("destroy");
    stump.doClick(1, 0);
    stump.doClick(3, 0);
    waitForIdle();
  } catch (e) {return `removeStump() e: ${e}`;}
  return null;
}
function dpo(s, l) {
  if ( (l > DEBUGLVL) || (!s) )
    return;
  s = `${s}`.padStart(l, '__');
  console.log(s);
  PBotUtils.sysMsg(s);
  PBotUtils.sleep(TICK/2);
}
function equipTool(item, slot) {
  dpo(`Switching to ${item}`, 1);

  if ( !unequipTools() ) {
    err("Unable to free active tool slots");
    return false;
  }
  let invItem = getItemFromInventory(item);
  if ( !invItem ) {
    err(`Unable to find ${item}`);
    return false;
  }
  let itemCoords = invItem.witem.c.div(33);
  invItem.takeItem(false);
  waitForHandEmpty(false, 5000);
  let equipory = PBotAPI.gui.getequipory();
  equipory.wdgmsg("drop", slot);
  waitForHandEmpty(true, TICK);
  if (waitForHandEmpty(false, 1000))
    PBotUtils.playerInventory().dropItemToInventory(itemCoords);
  return true;
}
function isEquiped(itemname) {
  let equipory = PBotAPI.gui.getequipory();
  let slots = [
  equipory.quickslots[6],
  equipory.quickslots[7]
  ];
  for (let i of slots) 
    if (i != null)
      if (i.item.getname() == itemname)
        return true;
      return false;
}
function unequipTools() {
  dpo("Unequiping tools", 2);
  if (!unequipSlot(6) || !unequipSlot(7))
    return false;
  dpo("Unequiping tools done", 2);
  return true;
}
function unequipSlot(n) {
  const TO = 5 * 1000;
  let equipory = PBotAPI.gui.getequipory();
  dpo(`Unequiping slot ${n}`, 3);
  let timer = TO;
  while ((equipory.quickslots[n] != null) && (timer >= 0)) {
    equipory.quickslots[n].item.wdgmsg("transfer", defCoord);
    timer -= TICK;
    dpo(`unequipTools() waiting: ${timer}`, 3);
    PBotUtils.sleep(TICK);
  }
  dpo(`Slot ${n}: ${equipory.quickslots[n]}`, 3);
  return (equipory.quickslots[n] == null);
}
function getItemFromInventory(itemname) {
  let ret = null;
  dpo(`Looking for ${itemname} in inventory`, 2);
  try {
    let items = PBotUtils.playerInventory().getInventoryItemsByNames([itemname]);
    dpo(`${items.length} ${itemname} found`, 3);
    if (items.length <= 0) return null;
    ret = PBotUtils.playerInventory().getInventoryItemsByNames([itemname])[0];
  } catch(e) {
    dpo(`getItemFromInventory(${itemname}) e: ${e}`, 1);
  }
  return ret;
}
function waitForHandEmpty(b, t){
  let counter = 0;
  while ((PBotUtils.getItemAtHand() == null) != b) {
    if (counter >= t)
      return false;
    PBotUtils.sleep(TICK);
    counter += TICK;
  }
  return true;
}
function err(s) {
  stopmsg.push(s);
}
function irq() {
  if (PBotCharacterAPI.getStamina() < 35) {
    err("Out of stamina");
  }
  if (PBotCharacterAPI.getEnergy() < 35) {
    err("Out of energy");
  }
  if (PBotWindowAPI.getWindow(WINDOWNAME) == null) {
    dpo("Stopped by user", 0);
    cancelAction();
    return true;
  }
  if (stopmsg.length != 0) {
    cancelAction();
    let errstr = "⚠ ";
    for (let r of stopmsg) {
      errstr += r + " ⚠ ";
    }
    PBotUtils.sysMsg(`${errstr}`, 192, 32, 32);
    return true;
  }
  return false;
}
function autodrinkSwitch(b) {
  if (Java.type('haven.Config').autodrink == b)
    return;
  Java.type('haven.Config').autodrink = b;
  dpo(`Autodrink ${b ? 'Enabled!' : 'Disabled!'}`, 1);
}
function cancelAction() {
  dpo("Canceling", 3);
  let aim = PBotGobAPI.player().gob.rc.floor();
  PBotUtils.mapClick(aim.x, aim.y, 1, 0);
}