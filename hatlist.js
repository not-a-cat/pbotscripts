const hatlist = {
	"ledcap" : {c:"LED Cap", at:"h"},
	"poachershat" : {c:"Poacher's Hat", at:"h"},
	"chantrellehat" : {c:"Chantrelle Hat", at:"h"},
	"lightningbowler" : {c:"Lightning Bowler", at:"h"},
	"tomatogrowershat" : {c:"Tomato Grower's Hat", at:"h"},
	"zebrarodeo" : {c:"Zebra Rodeo", at:"h"},
	"chainstove" : {c:"Checkers & Chains", at:"h"},
	"birchhat" : {c:"Birch Hat", at:"h"},
	"sleuthshat" : {c:"Sleuth's Hat", at:"h"},
	"jesterscap" : { c:"Jester's Cap", at:"h"},
	"blackguardsmanscap" : { c:"Black Guardsman's Cap", at:"h"},
	"deputyshat" : { c:"Deputy's Hat", at:"h"},
	"redcorsairshat" : { c:"Red Corsair's Hat", at:"h"},
	"inquisitorshat" : { c:"Inquisitor's Hat", at:"h"}
}