const hatlist = {
	"Warlock's Familiar" : 				{r : "hats/warlocksfamiliar", at:["h"]},
	"Painter's Beret" : 				{r : "hats/paintersberet", at:["h"]},
	"LED Cap" : 				{r : "ledcap", at:["h"]},
	"Poacher's Hat" : 				{r : "poachershat", at:["h"]},
	"Chantrelle Hat" : 				{r : "chantrellehat", at:["h"]},
	"Lightning Bowler" : 				{r : "lightningbowler", at:["h"]},
	"Tomato Grower's Hat" : 				{r : "tomatogrowershat", at:["h"]},
	"Zebra Rodeo" : 				{r : "zebrarodeo", at:["h"]},
	"Checkers & Chains" : 				{r : "chainstove", at:["h"]},
	"Birch Hat" : 				{r : "birchhat", at:["h"]},
	"Sleuth's Hat" : 				{r : "sleuthshat", at:["h"]},
	"Jester's Cap" : 				{r : "jesterscap", at:["h"]},
	"Black Guardsman's Cap" : 				{r : "blackguardsmanscap", at:["h"]},
	"Deputy's Hat" : 				{r : "deputyshat", at:["h"]},
	"Red Corsair's Hat" : 				{r : "redcorsairshat", at:["h"]},
	"Inquisitor's Hat" : 				{r : "inquisitorshat", at:["h"]}
}